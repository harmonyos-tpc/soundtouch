////////////////////////////////////////////////////////////////////////////////
///
/// Example class that invokes native SoundTouch routines through the JNI
/// interface.
///
/// Author        : Copyright (c) Olli Parviainen
/// Author e-mail : oparviai 'at' iki.fi
/// WWW           : http://www.surina.net
///
////////////////////////////////////////////////////////////////////////////////

package net.surina.soundtouch;

import ohos.aafwk.ability.AbilityPackage;

public final class SoundTouchExample extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        SoundTouch.Instance();
    }

    @Override
    public void onEnd() {
        SoundTouch.destroy();
        super.onEnd();
    }
}
