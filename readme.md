SoundTouch
===============

## Introduction
SoundTouch is an open-source audio processing library for changing the Tempo, Pitch and Playback Rates of audio streams or audio files. The library additionally supports estimating stable beats-per-minute rates for audio tracks.

**Tempo (time stretch):** Changes the sound to play at faster or slower tempo than originally without affecting the sound pitch.
**Pitch (key) :** Changes the sound pitch or key while keeping the original tempo (speed).
**Playback Rate :** Changes both tempo and pitch together as if a vinyl disc was played at different RPM rate.
The SoundTouch library is intended  for application developers writing sound processing tools that require tempo/pitch control functionality, or just for playing around with the sound effects.

## Usage Instructions

Change Sound tempo, pitch and playback rate parameters independently from each other:

* Change **tempo** while maintaining the original pitch
* Change **pitch** while maintaining the original tempo
* Change **playback rate** that affects both tempo and pitch at the same time
* Change any combination of tempo/pitch/rate

Visit [SoundTouch website](https://www.surina.net/soundtouch) for more information and audio examples.

## Installation Instructions

```
method 1: Generate har package from library add it to the libs folder
		add the following code to gradle of entry 
		
		implementation fileTree(dir: 'libs', include: ['*.har'])

method 2:
allprojects {
    repositories {
        mavenCentral()
    }
}

dependencies {
    implementation 'io.openharmony.tpc.thirdlib:soundtouch:1.0.0'
}
```

## LICENSE
SoundTouch is released under LGPL v2.1:

This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License version 2.1 as published by the Free Software Foundation.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

See [LGPL v2.1 full license text ](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html) for details.

--

Also commercial license free of GPL limitations available upon request
